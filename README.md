Code to carry out analyses in:

The development of scientific consensus: analyzing conflict and concordance among Avian phylogenies
Joseph Brown, Ning Wang, Stephen Smith

Preprint: http://biorxiv.org/content/early/2017/03/31/123034

Note: Code in dir `scripts_for_ref_taxonomy` is also present in `reference-taxonomy`.

To run you will need to do the following:

1. Decompress the archive `reference-taxonomy.zip`
2. Clone the Open Tree study repo "phylesystem-1":
    git clone git@github.com:OpenTreeOfLife/phylesystem-1.git
3. Run as:
    bin/jython conflict.py --shard location/of/phylesystem-1/ --out conflict.outfilename --ref reference/tree/location --srclist python_tree_list > conflict.ifs.outfilename
4. Parse the conflict:
    python process_conflict_out.py conflict.ifs > conflict.out
5. Annotate the reference tree (using `pxtcol` from the `phyx` package available [here](https://github.com/FePhyFoFum/phyx)):
    pxtcol -t reference/tree/location -d conflict.out -o annotated_reference_tree_name
6. Finally, annotate the original tree. Smaller tree must be labelled with ottid tips. Easy to use the `grafted_solution.tre` from the propinquity output.
    python map_two_trees.py annotated_reference_tree_name grafted_solution_tree annotated_source_tree_name

Here is a complete example running the analysis against the Jarvis et al. (2014) tree. Assumes 1) running from the `reference-taxonomy` directory, and 2) reference trees reside in the directory `../ref_trees/`:
```
bin/jython conflict.py --out conflict.jarvis --shard ../phylesystem-1/ --ref ../ref_trees/Jarvis-only_synth.tre --srclist studytreelist_11Nov2016.py > conflict.ifs
python process_conflict_out.py conflict.ifs > conflict.out
pxtcol -t ../ref_trees/Jarvis-only_synth.tre -d conflict.out -o ../ref_trees/Jarvis-only_synth.conflict.ann.tre
python map_two_trees.py ../ref_trees/Jarvis-only_synth.conflict.ann.tre ../ref_trees/Jarvis_grafted_solution.tre Jarvis.conflict.ann.tre
```
