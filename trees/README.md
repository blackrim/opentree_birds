# Notes on the trees in this directory

Note: Each of the trees in this directory (save the new synthetic tree) is a 
synthesis between the original published phylogenetic hypothesis and the Open 
Tree of Life Taxonomy (OTT) so that all trees have comparable leaf sets. The 
node labels in these trees correspond to Open Tree of Life taxonomic IDs 
(ottids) if they correspond to a taxon in OTT or an OTT MRCA statement (e.g. 
mrcaott4131650ott4131653) if the node is not a named taxon.

We also provide a version of the new synthetic tree 
('Brown_et_al_2017_New_synthetic_tree_taxlabels.tre') with the labels 
'ottname ottid' for named taxa and an OTT MRCA statement if the node is not a 
named taxon.
