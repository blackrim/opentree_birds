import sys

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "python "+sys.argv[0]+" infile.out"
        sys.exit(0)

    inf = open(sys.argv[1],"r")
    nd_d = {}
    for i in inf:
        if i[0:3] != "IFS":
            continue
        i = i.strip().split("IFS:")[1].replace("(","").replace(")","")
        if "SUPPORTED_BY" in i:# or "PATH_SUPPORTED_BY" in i:
            spls = i.split(" ")
            if "ot_" in spls[1]:
                continue
            name = ""
            if "null" in spls[0]:
                name = spls[1].replace(":","").split("+")[0]
            else:
                name = spls[0]
            if name not in nd_d:
                nd_d[name] = 0.01
            nd_d[name] += 1.
    inf.close()

    for i in nd_d:
        print i+"\t"+str(nd_d[i])
