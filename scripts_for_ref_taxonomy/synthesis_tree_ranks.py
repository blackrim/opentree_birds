studytreelist = [

##########
# NOTES:
# trees indicated with *NEW* have not been a part of synthesis previously
# commented-out trees are excluded from synthesis. the reason follows the citation
##########

## Palaeognaths ##
"pg_2876@tree6670", # Tinamidae. Bertelli et al. 2004. Orn. Neotrop.
"pg_2926@tree6757", # Palaeognaths. Mitchell et al. 2014. Science
#"pg_2580@tree5986", # Palaeognathae. Smith et al. 2013. Syst. Biol. *NEW* sparse sampling
#"pg_2800@tree6501", # Palaeognathae. Harshman et al. 2008. PNAS *NEW* overruled by newer study


## Galliformes ##
"ot_120@tree1",     # Cracidae. Pereira and Baker. 2004. Auk *NEW*
"ot_119@tree1",     # Cracidae. Pereira et al. 2002. Syst. Biol.
"ot_118@tree1",     # Megapodidae. Harris et al. 2014. J. Biogeo.
"ot_425@tree1",     # Galliformes. Stein et al. 2015. MPE *NEW* 229 taxa
"ot_753@tree1",     # Galliformes. Wang et al. 2016. J. Biogeo. *NEW* 93 taxa
"pg_2804@tree6511", # Galliformes. Kimball et al. 2011. Int. J. Evol. Biol. *NEW* 170 taxa
"ot_1002@tree1",    # Galliformes. Hosner et al. 2016. MBE *NEW* 91 taxa
#"pg_2577@tree5980", # Galliformes. Wang et al. 2013. PLoS ONE 92 taxa superceded by 2016 paper
#"ot_791@Tr3192",    # Galliformes. Ksepka. 2009. Cladistics *NEW* (combined morph + DNA) 64 taxa. other studies have better sampling
#"ot_883@tree1",     # Callipepla. Zink and Blackwell. 1998. Auk *NEW* newer studies available
#"ot_915@tree1",     # Polyplectron. Chang et al. 2008. Zool. Sci. *NEW* small sample
#"ot_792@Tr93563",   # Galliformes. Ksepka. 2009. Cladistics *NEW* (morphology)
#"pg_2583@tree5989", # Galliformes. Eo et al. 2009. Zool. Scripta *NEW* supertree
#"pg_2801@tree6503", # Galliformes. Cox et al. 2007. Auk *NEW* overruled by newer studies
#"pg_2802@tree6506", # Galliformes. Kimball and Braun. 2008. J. Av. Biol. *NEW* overruled by newer studies
#"pg_2803@tree6508", # Galliformes. Bonilla et al. 2010. MPE *NEW* overruled by newer studies



## Anseriformes ##
"ot_731@tree2",     # Anatidae (geese). Ottenburghs et al. 2016. MPE *NEW*
"pg_2866@tree6656", # Anseriformes. Gonzalez et al. 2009. J. Zool.
"pg_2860@tree6646", # Anatidae. Fulton et al. 2012. Proc. Roy. Soc.
#"ot_780@Tr3788",    # Anas. Kennedy and Spencer. 2000. Auk *NEW* small sample
#"ot_817@tree1",     # Anatidae (geese). Paxinos et al. 2002. PNAS *NEW* newer available
#"ot_887@tree1",     # Anatidae. Zimmer et al. 1994. MPE *NEW* newer studies available
#"pg_2459@tree5282", # Anas. Peters et al. 2005. MPE *NEW* small sample


## Piciformes ##
"ot_150@tree3",     # Megalaimidae. den Tex and Leonard. 2013. MPE
"ot_870@tree1",     # Megalaima. Feinstein et al. 2007. Ibis *NEW*
"ot_156@tree1",     # barbets. Moyle. 2004. MPE
"ot_153@tree1",     # Pteroglossus. Patel et al. 2011. MPE
"ot_830@tree1",     # Pteroglossus. Pereira and Wajntal. 2008. Gen. Mol. Biol. *NEW*
"ot_151@tree1",     # Capito. Armenta et al. 2005. Condor
"ot_154@tree1",     # Andigena+Selenidera. Lutz et al. 2013. MPE
"ot_155@tree1",     # Aulacorhynchus. Bonaccorso et al. 2011. Zool. Scripta
"ot_152@tree1",     # Ramphastos. Patane et al. 2009. MPE
"ot_157@tree1",     # Celeus. Benz and Robbins. 2011. MPE
"ot_158@tree1",     # Colaptes. Moore et al. 2011. MPE
"ot_504@tree3",     # Picidae. Dufort. 2015. MPE *NEW*
"ot_159@tree1",     # Picidae. Benz et al. 2006. MPE
"ot_794@tree2",     # Picidae. Fuchs et al. 2007. Zool. Scripta *NEW*
#"ot_781@Tr48743",   # Piciformes. Barker and Lanyon. 2000. MPE *NEW* overruled by newer studies
#"ot_829@tree1",     # Picidae. Moore. 1995. Evolution *NEW* newer studies available


## Charadriiformes ##
"ot_144@tree1",     # Alcidae. Pereira and Baker. 2008. MPE
"ot_136@tree1",     # Jacanidae. Whittingham et al. 2000. Auk
"ot_289@tree1",     # Charadrius. Dos Remedios et al. 2015. MPE *NEW*
"ot_873@tree1",     # Stercorariidae. Blechschmidt et al. 1993. Zool. J. Syst. *NEW* old but currently unsampled
"ot_816@tree1",     # Charadriiformes. Gibson and Baker. 2012. MPE *NEW*
"ot_146@tree1",     # Charadriiformes. Baker et al. 2007. Biol. Lett. *NEW*
"pg_2371@tree4985", # Scolopacidae. Ammon and Ellegren. 2013. MPE *NEW* small sample, but currently unsampled


## Accipitriformes + Cathartidae ##
"ot_305@tree1",     # Circus. Oatley et al. 2015. MPE *NEW*
"pg_2857@tree6628", # Gyps. Arshad et al. 2009. J. Ornith.
"pg_1887@tree6629", # Accipitridae. Lerner et al. 2008. Auk
"ot_812@tree4",     # Cathartidae. Johnson et al. 2016. MPE *NEW*
#"ot_857@tree1",     # Buteo. Hull et al. 2008. Biol. J. Linn. Soc. *NEW* small sample

## Columbiformes ##
"pg_2850@tree6620", # Columbidae. Cibois et al. 2014. MPE
"ot_655@tree1",     # Columbidae. Besnard et al. 2015. Biol. J. Linn. Soc *NEW* (nucDNA)
#"ot_655@tree2",     # Columbidae. Besnard et al. 2015. Biol. J. Linn. Soc *NEW* (mtDNA) overruled by nucDNA
"pg_2444@tree6526", # Columbiformes. Pereira et al. 2007. Syst. Biol.
"ot_769@tree3",     # Columbidae. Sweet and Johnson. 2015. Auk *NEW*


## Falconidae ##
"pg_2870@tree6662", # Falconidae. Fuchs et al. 2011. MPE
"pg_2871@tree6663", # Falconidae. Fuchs et al. 2012. Ibis


## Psittaciformes ##
"ot_166@tree1",     # Androglossini. Quintero et al. 2012. Zool. Scripta
"ot_161@tree1",     # Amazona. Russello and Amato. 2004. MPE
"ot_160@tree1",     # Cacatuidae. White et al. 2011. MPE
"ot_162@tree1",     # Prioniturus. Schweizer et al. 2012. J. Zool. Syst. Evol. Res.
"ot_164@tree1",     # Brotogeris. Ribas et al. 2009. J. Biogeo.
"ot_165@tree1",     # Forpus. Smith et al. 2012. Mol. Ecol.
"ot_167@tree1",     # Pyrrhura. Ribas et al. 2006. Auk
"ot_313@tree2",     # Platycercus. Shipham et al. 2015. MPE *NEW*
"pg_1979@tree6300", # Psittacidae. Tavares et al. 2006. Syst. Biol.
"ot_312@tree1",     # Loriidae. Schweizer et al. 2015. MPE *NEW*
"pg_2805@tree6512", # Psittaciformes. Wright et al. 2008. MBE
"pg_2806@tree6529", # Psittaciformes. Joseph et al. 2011. MPE *NEW*
#"ot_871@tree1",     # Psittaciformes. Birt et al. 1992. Hereditas *NEW* small sample
#"ot_871@tree2",     # Psittaciformes. Birt et al. 1992. Hereditas *NEW* small sample
#"ot_921@tree1",     # Psittaciformes. Tokita et al. 2007. Evol. Devel. *NEW* newer studies available


## Apodiformes + friends ##
"ot_112@tree1",     # Apodidae. Packert et al. 2012. MPE
"pg_2853@tree6624", # Trochilidae. McGuire et al. 2014. Curr. Biol.
"pg_2865@tree6654", # Cynanthus. Garcia-Deras et al. 2008. Zootaxa. *** monophyly rejected by pg_2853_6624 *NEW*
"ot_123@tree1",     # Aegotheles. Dumbacher. 2003. MPE
"pg_2872@tree6665", # Caprimuligidae. Han et al. MPE # can do better than this one?
#"pg_2158@tree4551", # Coeligena. Parra et al. 2009. MPE *NEW* overruled by newer studies
#"pg_2658@tree6192", # Trochilidae. McGuire et al. 2007. Syst. Biol. *NEW* newer study from same authors available


## Gruiformes ##
"ot_125@tree1",     # Psophia. Ribas et al. 2011. Proc. Roy. Soc.
"ot_126@tree1",     # Gruidae. Krajewski et al. 2010. Auk *NEW*
"ot_415@tree2",     # Gruiformes. Garcia et al. 2014. PLoS ONE *NEW*
"pg_1872@tree3780", # Gruiformes. Fain et al 2007. MPE
#"ot_865@tree1",     # Gallinula. Dick et al. 2008. PLoS ONE *NEW* only 2 taxa


## Sphenisciformes ##
"ot_806@tree2",     # Sphenisciformes. Gavryushkina et al. 2016. Syst. Biol. *NEW*
"pg_2798@tree6497", # Spheniscidae. Subramanian et al. 2013. Biol. Lett.
"ot_786@Tr53559",   # Sphenisciformes. Ksepka et al. 2012. J. Vert. Paleo. *NEW*
#"ot_872@tree1",     # Sphenisciformes. Bertelli and Giannini. 2005. Cladistics *NEW* newer studies available


## waterbirds ##
"pg_1764@tree6299", # Pelicanidae. Kennedy et al. 2013. MPE
"ot_137@tree1",     # Sulidae. Patterson et al. 2011. MPE
"ot_140@tree1",     # Phalacrocoracidae. Kennedy and Spencer. 2014. MPE
"ot_104@tree1",     # Gavia. Boertmann. 1990. Steenstrupia
"ot_142@tree1",     # Diomedeidae. Chambers et al. 2009. Notornis
"ot_138@tree1",     # Ciconiidae. Slikas. 1997. MPE
"pg_2864@tree6651", # Platalea. Chesser et al. 2010. Zootaxa
"ot_820@tree2",     # Pterodroma. Zino et al. 2008. Ibis *NEW*
"ot_139@tree1",     # Threskiornithidae. Ramirez et al. 2013. Gen. Mol. Res.
"ot_862@tree1",     # Ardeidae. Zhou et al. 2015. J. Ornith. *NEW*
"pg_2416@tree3",    # Procellariiformes. Nunn and Stanley. 1998. MBE *NEW*
#"ot_776@Tr6803",    # Phalacrocoracidae. Holland et al. 2010. Syst. Biol. *NEW* (next to no resolution)
#"ot_777@Tr77888",   # Phalacrocoracidae. Holland et al. 2010. Syst. Biol. *NEW* (doesn't seem to exist)
#"ot_779@Tr403",     # Phalacrocoracidae. Kennedy et al. 2000. MPE *NEW* overruled by newer studies
#"pg_2152@tree4545", # Phalacrocoracidae. Kennedy et al. 2009. MPE *NEW* overruled by newer studies
#"ot_775@Tr289",     # Pelecaniformes. Kennedy et al. 2004. MPE *NEW* (monophyletic Pelecaniformes)
#"ot_778@Tr2093",    # Pelecaniformes. Kennedy et al. 2005. Syst. Biol. *NEW* small sample
#"ot_844@tree1",     # Diomedeidae. Nunn et al. 1996. Auk *NEW* newer studies available
#"pg_2528@tree5445", # Procellariiformes. Kennedy and Page. 2002. Auk *NEW* supertree
#"ot_861@tree3",     # Ardeidae. McCracken and Sheldon. 1997. PNAS *NEW* DNA-DNA hybridization
#"ot_860@tree1",     # Ardeidae. McCracken and Sheldon. 1998. MPE *NEW* newer studies available


## Strigiformes ##
"ot_533@tree2",     # Megascops. Dantas et al. 2016. MPE *NEW*
"ot_773@tree1",     # Strigidae. Wood et al. 2016. Zool. J. Linn. Soc. *NEW*
"ot_867@tree2",     # Strigidae. Fuchs et al. 2008. BMC Evol. Biol. *NEW*
"ot_116@tree1",     # Strigiformes. Hugall and Stuart-Fox. 2012. Nature


## Coraciiformes ##
"ot_148@tree1",     # Meropidae. Marks et al. 2007. MPE
"ot_149@tree4",     # Todidae. Overton et al. 2004. MPE
"ot_835@tree1",     # Alcedinidae. Moyle et al. 2007. J. Av. Biol. *NEW*
"ot_837@tree1",     # Alcedinidae. Moyle et al. 2006. Auk *NEW*
"ot_846@tree1",     # Alcedinidae. Melo and Fuchs. 2008. Ibis. *NEW*


## various Neognathae ##
"ot_121@tree7",     # Cuculiformes. Payne. 2005. Book
"pg_2869@tree6661", # Bucerotiformes. Gonzalez et al. 2013. MPE
"ot_122@tree1",     # Phoenicopteriformes. Torres et al. 2014. BMC Evol. Biol.
"ot_864@tree1",     # Podicipedidae. Ogawa et al. 2015. Auk *NEW*
"ot_124@tree1",     # Otididae. Pitra et al. 2002. MPE
"ot_129@tree1",     # Musophagidae. Njabo and Sorenson. 2009. Ostrich
"ot_147@tree1",     # Trogonidae. Ornelas et al. 2009. J. Evol. Biol.
"ot_838@tree1",     # Trogoniformes. Moyle. 2005. Biol. J. Linn. Soc. *NEW*
#"ot_913@tree1",     # Trogon. Dacosta and Klicka. 2008. Mol. Ecol. *NEW* covered by other studies
#"ot_821@tree1",     # Coliiformes. Zelenkov and Dyke. 2008. Palaeontology *NEW* (next to no resolution)


###################
## Passeriformes ##
###################

# Acanthisittidae
"ot_747@tree3",     # Acanthisittidae. Mitchell et al. 2016. MPE *NEW*

################################
### * Tyranni (suboscines) * ###

# Pipridae
"pg_2796@tree6491", # Pipridae. Ohlson et al. 2013. MPE *NEW*

# Furnariidae
"pg_1953@tree3977", # Furnariidae. Derryberry et al. 2011. Evolution *NEW*
"ot_866@tree1",     # Furnariidae. Gonzalez and Wink. 2008. J. Ornith. *NEW*

# Thamnophilidae
"pg_2454@tree5247", # Thamnophilus. Brumfield and Edwards. 2007. Evolution

# Formicariidae
"pg_2890@tree6701", # Formicariidae. Rice. 2005. Auk *NEW*

#"ot_876@tree1",     # Tyrannidae. Zink and Johnson. 1984. Syst. Biol. *NEW* newer studies available
#"ot_875@tree1",     # Furnariidae. Zyskowski and Prum. 1999. Auk *NEW* newer studies available
#"pg_1766@tree3562", # Furnariidae. Claramunt et al. 2012. Am. Nat. *NEW* reanalysis
#"pg_2929@tree6765", # Synallaxis (Furnariidae). Claramunt. 2014. MPE *NEW* covered by other studies
#"ot_917@tree1",     # Xiphorhynchus. Cabanne et al. 2008. MPE *NEW* newer studies available

#### end Tyranni ####
#####################

###################################
## below are songbirds (oscines) ##

###########################
## basal oscine lineages ##

# Ptilonorhynchidae
"ot_815@tree2",     # Sericulus. Zwiers et al. 2008. MPE *NEW*

# Maluridae
"pg_1966@tree4019", # Maluridae. Lee et al. 2011. Syst. Biol.

## end basal ##
###############

#################################
### * superfamily Corvoidea * ###

# Rhipiduridae
"ot_174@tree1",     # Rhipidura. Nyari et al. 2009. Zool. Scripta

# Vireonidae
"ot_768@tree1",     # Vireonidae. Slager et al. 2014. MPE *NEW* (ND2)
"ot_824@tree1",     # Pteruthius. Reddy. 2008. MPE *NEW*
#"ot_768@tree2",     # Vireonidae. Slager et al. 2014. MPE *NEW* (z-linked) using alternate mtDNA tree

# Oriolidae
"pg_2875@tree6668", # Oriolidae. Jonsson et al. 2010. Ecography

# Paradisaeidae
"ot_763@tree1",     # Paradisaeidae. Irestedt et al. 2009. BMC Evol. Biol. *NEW*
"ot_823@tree1",     # Parotia. Scholes. 2008. Biol. J. Linn. Soc. *NEW*

# Pachycephalidae
"ot_853@tree1",     # Pachycephala. Knud et al. 2008. J. Av. Biol. *NEW*

"pg_2702@tree6274", # core Corvoidea. Aggerbeck et al. 2014. MPE *** mapping back to Passeroidea *NEW*
"ot_520@tree1",     # Corvoidea. Jonsson et al. 2016. MPE *NEW*
"ot_863@tree2",     # Corvidae (NZ). Scofield et al. 2016. MPE *NEW*
#"ot_890@tree1",     # Aphelocoma (Corvidae). Delaney et al. 2008. Auk *NEW* only 2 taxa in ingroup

#### end superfamily Corvoidea #####
####################################

###############################
### * parvorder Passerida * ###

##################
# Certhioidea (previously Sylvioidea) #

# Certhiidae
"ot_172@tree1",     # Certhia. Packert et al. 2011. J. Biogeo.
#"ot_826@tree1",     # Certhia. Tietze et al. 2008. Org. Div. Evol. *NEW* overruled by newer studies
#"pg_2873@tree6666", # Certhia. Tietze et al. 2006. Ibis *NEW* overruled by newer studies

# Sittidae
"pg_2845@tree6606", # Sitta. Pasquet et al. 2014. J. Ornith.
#"ot_828@tree1",     # Sitta. Pasquet. 2008. Ibis *NEW* newer studies available

# Troglodytidae
"pg_1854@tree3743", # Troglodytinae. Mann et al. 2006. MPE *NEW*

# Polioptilidae
"ot_882@tree1",     # Polioptila. Zink and Blackwell. 1998. MPE *NEW* old but currently unsampled

## end Certhioidea ##
#####################

##########################
# superfamily Sylvioidea #

# Alaudidae
"pg_2599@tree6021", # Alaudidae. Alstrom et al. 2013. MPE
#"ot_858@tree1",     # Galerida. Guillaumet et al. 2008. BMC Evol. Biol. *NEW* small sample

# Hirundinidae
"ot_176@tree1",     # Tachycineta. Cerasale et al. 2012. MPE
"ot_177@tree1",     # Hirundo. Dor et al. 2010. MPE
"ot_834@tree1",     # Progne. Moyle et al. 2008. Wilson J. Ornith *NEW*
"ot_178@tree1",     # Hirundinidae. Sheldon et al. 2005. MPE

# Acrocephalidae
"ot_103@tree4",     # Acrocephalinae. Arbabi et al. 2014. MPE *NEW*
"ot_914@tree1",     # Acrocephalus. Cibois et al. 2008. Ibis *NEW*

# Pycnonotidae
"ot_832@tree1",     # Pycnonotidae. Moyle and Marks. 2006. MPE *NEW*

# Timaliidae
"pg_1586@tree3208", # Timaliidae. Moyle et al. 2012. Syst. Biol. *NEW*

# Sylviidae
"pg_2332@tree4912", # Sylviidae. Voelker and Light. 2011. BMC Evol. Biol. *** Pseudoalcippe should be in Silviidae rather than Timaliidae *NEW*

# Cettiidae
"pg_2105@tree4360", # Passeriformes (Cettia). Lecroy and Barker. 2006. Am. Mus. Novitates *NEW*

## end Sylvioidea ##
####################

#############################
# superfamily Muscicapoidea #

# Muscicapidae (Old World flycatchers)
"ot_822@tree1",     # Saxicola. Woog et al. 2008. J. Ornith. *NEW*
"ot_856@tree1",     # Saxicola. Illera et al. 2008. MPE *NEW*
"ot_110@tree1",     # Oenanthe. Schweizer and Shirihai. 2013. MPE
"ot_894@tree1",     # Oenanthe. Aliabadian et al. 2007. MPE *NEW* small sample, but only partially sampled currently
"ot_749@tree1",     # Muscicapidae. Hooper et al. 2016. MPE *NEW*

# Sturnidae
"ot_848@tree1",     # Sturnidae. Lovette et al. 2008. MPE *NEW*
"ot_180@tree1",     # Sturnidae. Lovette et al. 2007. MPE *NEW*

# Mimidae
"ot_179@tree1",     # Mimidae. Lovette et al. 2012. MPE *NEW*
"ot_880@tree1",     # Toxostoma. Zink et al. 1999. Auk *NEW* old but currently unsampled

# Turdidae
"pg_2858@tree6642", # Turdidae. Nylander et al. 2008. Syst. Biol. *NEW*
#"ot_920@tree1",     # Turdus. Voelker et al. 2007. MPE *NEW* newer study from same authors available

## end Muscicapoidea ##
#######################

###########################
# superfamily Passeroidea #

# Fringillidae
"ot_827@tree1",     # Loxia. Parchman et al. 2007. Evolution *NEW*
"pg_2874@tree6667", # Carpodacus. Tietze et al. 2013. Zool. J. Linn. Soc. * not monophyletic *NEW*
"ot_172@tree2",     # Pinicola+Pyrrhula. Packert et al. 2011. J. Biogeo.
"pg_2692@tree6245", # Fringillidae. Zuccon et al. 2012. MPE *NEW*
#"ot_840@tree1",     # Fringillidae. Yuri and Mindell. 2002. MPE *NEW* overruled by newer studies

# Drepanidinae
"ot_113@tree1",     # Drepanidinae (honeycreepers). Lerner et al. 2011. Curr. Biol. *NEW*

# Icteridae
"pg_2707@tree6281", # Icteridae. Powell et al. 2013. MPE *** Hypopyrrhus not mapped to Icteridae.
#"ot_845@tree2",     # Quiscalus. Powell et al. 2008. Condor *NEW* newer studies available
#"ot_918@tree2",     # Sturnella. Barker et al. 2008. Auk *NEW* small sample

# Parulidae
"pg_2591@tree6024", # Parulidae. Lovette et al. 2008. MPE
#"ot_825@tree1",     # Setophaga. Rabosky and Lovette. 2008. Proc. Roy. Soc. *NEW* reanalysis

# Thraupidae
"ot_891@tree1",     # Darwin's finches. Lamichhaney et al. 2015. Nature. *NEW*
"pg_2829@tree6579", # Thraupidae. Burns et al. 2014. MPE *NEW*
"ot_101@tree1",     # Saltator. Chaves et al. 2013. J. Biogeo. *** Includes Saltatricula (Thraupini), while Saltator is Cardinalini *NEW*

# Cardinalidae
"ot_102@tree1",     # Pheucticus. Pulgarin-R et al. 2013. MPE
"ot_169@tree1",     # Cardinalidae. Bryson et al. 2014. J. Biogeo. *NEW*
#"ot_916@tree1",     # Passerina. Carling and Brumfield. 2008. Genetics *NEW* small sample

# Emberizidae
"pg_2924@tree6755", # Emberizinae. Klicka et al. 2014. MPE *NEW*
#"ot_877@tree1",     # Pipilo. Zink et al. 1998. MPE *NEW* newer studies available
#"ot_878@tree1",     # Pipilo. Zink and Dittmann. 1991. Condor. *NEW* newer studies available
#"ot_879@tree1",     # Spizella. Zink and Dittmann. 1993. Wilson Bull. *NEW* newer studies available
#"ot_884@tree1",     # Ammodramus. Zink and Avise. 1990. Syst. Zool. *NEW* newer studies available
#"ot_884@tree2",     # Ammodramus. Zink and Avise. 1990. Syst. Zool. *NEW* newer studies available
#"ot_886@tree1",     # Zonotrichia. Zink et al. 1991. Auk *NEW* newer studies available
#"ot_919@tree3",     # Emberizini. Alstrom et al. 2008. MPE *NEW* newer more resolved studies available
#"ot_881@tree1",     # Passerellidae. Zink and Blackwell. 1996. Auk *NEW* newer studies available
#"ot_885@tree1",     # Passerellidae. Zink. 1982. Auk *NEW* newer studies available

## end Passeroidea ##
#####################

############################
# Passerida incertae sedis #

# Bombycillidae
"ot_171@tree1",     # Bombycillidae. Spellman et al. 2008. MPE *NEW*

# Regulidae
"ot_170@tree1",     # Regulus. Packert et al. 2009. J. Ornith.

# Paridae
"ot_173@tree1",     # chickadees. Harris et al. 2014. Evolution
"pg_2600@tree6022", # Paridae. Johansson et al. 2013. MPE

# Stenostiridae
"ot_175@tree1",     # Stenostiridae. Nguembock et al. 2008. Zool. Scripta *NEW*

## end incertae sedis ##
########################

#### end parvorder Paserida #####
#################################

"ot_783@tree1",     # Passeriformes. Moyle et al. 2016. Nature Comm. *NEW* 106 taxa
"ot_843@tree3",     # Passeriformes (Tyrannidae). Rheindt et al. 2008. MPE *NEW* 70 taxa
"ot_770@tree1",     # Passeriformes (New World). Barker et al. 2015. Auk *NEW* 795 taxa
"pg_2575@tree5974", # Passeriformes (New World). Barker et al. 2013. Syst. Biol. *NEW* 191 taxa
"pg_2913@tree6742", # Himilayan songbirds. Price et al. 2014. Nature *NEW* 482 taxa

"ot_111@tree1",     # Passeriformes. Alstrom et al. 2014. Biol. Lett. *NEW* 131 taxa
"ot_172@tree3",     # Passeriformes (Aegithalos). Packert et al. 2011. J. Biogeo. *NEW* 15 txa
"ot_290@tree1",     # Passeriformes. Selvatti et al. 2015. MPE *NEW* 1066 taxa (5 mtDNA, 4 nucDNA genes)
"pg_2015@tree4152", # Passeriformes. Odeen et al. 2011. Evolution *NEW* 56 taxa
"ot_854@tree1",     # Passeriformes. Johansson et al. 2008. MPE *NEW* 83 taxa
"ot_855@tree1",     # Passeriformes. Irestedt et al. 2008. MPE *NEW* 43 taxa
"ot_841@tree1",     # Passeriformes. Treplin et al. 2008. Cladistics *NEW* (tree is Bayesian) 82 taxa
"ot_836@tree1",     # Passeriformes. Moyle et al. 2006. MPE *NEW* 31 taxa
"ot_874@tree1",     # Passeriformes. Zuccon et al. 2006. MPE *NEW* 55 taxa

"pg_2404@tree5068", # Passeriformes. Barker et al. 2004. PNAS 146 taxa (this was the Passeriformes backbone previously)

# extra Passeriformes trees to pick up unsampled taxa
"ot_757@Tr70438",   # Passeriformes. Ericson et al. 2014. BMC Evol. Biol. *NEW* 55 taxa
"ot_412@Tr77157",   # Passeriformes. Barker. 2014. MPE *NEW* (mtDNA only) 125 taxa
"ot_532@tree1",     # Passeriformes. Gibb et al. 2015. GBE *NEW* (mtDNA only) 102 taxa 
"ot_782@Tr48736",   # Passeriformes. Beresford et al. 2005. Proc. Roy. Soc. *NEW* (parsimony consensus) 110 taxa
"ot_831@tree2",     # Passeriformes (Old World suboscines). Moyle et al. 2006. Am. Mus. Novitates *NEW* (parsimony) 19 taxa
"ot_869@tree1",     # Passeriformes. Dumbacher et al. 2008. MPE *NEW* 30 taxa. many non-monophyletic genera
"ot_847@tree1",     # Passeriformes (Vangidae). Manegold. 2008. J. Zool. Syst. Evol. Res. *NEW* (parsimony) 36 taxa

#"ot_852@tree1",     # Passeriformes. Knud et al. 2008. MPE *NEW* 85 taxa not great resolution
#"ot_787@Tr64310",   # Passeriformes. Alcaide et al. 2013. PeerJ *NEW* unreliable
#"ot_888@tree1",     # Passeriformes. Zhang et al. 2007. J. Ornith. *NEW* 20 taxa. newer studies available
#"pg_2125@tree4414", # Passeriformes. Lecroy and Barker. 2006. Am. Mus. Novitates *NEW* 24 taxa (doesn't seem to exist)
#"ot_839@tree1",     # Passeriformes. Yuri et al. 2008. MBE *NEW* 24 taxa. small sample
#"ot_842@tree2",     # Passeriformes. Treplin Tiedemann. 2007. MPE *NEW* (next to no resolution)

### end Passeriformes ###

## Aves ##
"ot_500@tree1",     # Aves. Prum et al. 2015. Nature *NEW* 198 taxa
"ot_531@tree1",     # Aves. Claramunt and Cracraft. 2015. Sci. Adv. *NEW* 229 taxa, 2 genes
"pg_2913@tree6741", # Non-passeriformes. Price et al. 2014. Nature *NEW* 176 taxa
"ot_809@tree2",     # Aves. Jetz et al. 2012. Nature *NEW* 6670  taxa. can add unsampled taxa
"ot_521@tree1",     # Aves. Burleigh et al. 2015. MPE *NEW* 6714  taxa, but sparse matrix
#"ot_314@tree1",     # Neognathae. Suh et al. 2015. PLoS Biol. *NEW* 48 taxa. retrotransposon presence/absence
#"ot_534@tree1",     # Aves. Ksepka and Phillips. 2015. Ann. MIss. Bot. Gard. *NEW* 72 taxa. good, but comprehensive studies available
#"pg_420@tree522",   # Aves. Hackett et al. 2008. Science 169 taxa. good, but newer studies available
#"ot_214@tree1",     # Aves. Jarvis et al. 2014. Science *NEW* 48 taxa


## DO NOT USE e.g. supertree, reanalysis, overruled by other studies, small sample, etc.
#"ot_851@tree1",     # Neognathae. Lee et al. 2008. Electrophoresis *NEW* (root is wrong)
#"pg_2616@tree6487", # Neognathae. Gibb et al. 2013. MPE *NEW* 48 taxa. overruled by newer studies
#"pg_2797@tree6493", # Neognathae. McCormack et al. 2013. PLoS ONE *NEW* newer studies have better sampling
#"ot_117@tree1",     # Aves. Brown et al. 2007. Biol. Lett. *NEW* reanalysis
#"ot_409@tree2",     # Life (Aves). Hedges et al. 2015. MBE *NEW* supertree
#"ot_427@tree5",     # Aves. Sibley and Ahlquist. 1990. book *NEW* overruled by newer studies
#"ot_428@tree1",     # Aves. Livezey and Zusi. 2007. Zool. J. Linn. Soc. *NEW* overruled by newer studies
#"ot_429@tree1",     # Aves. Davis and Page. 2014. PLoS Curr. ToL *NEW* supertree
#"ot_784@Tr25484",   # Aves. Han et al. 2011. Syst. Biol. *NEW* reanalysis of Hackett tree
#"ot_798@tree1",     # Aves. McClelland et al. 2016. Physio. Biochem. Zool. *NEW* can't get/judge paper
#"ot_818@tree1",     # Aves. Mindell et al. 1999. Syst. Biol. *NEW* overruled by newer studies
#"ot_819@tree1",     # Aves. Bleiweiss et al. 1995. Auk *NEW* overruled by newer studies
#"pg_2799@tree6500", # Aves. Chojnowski et al. 2008. Gene *NEW* newer studies have better sampling
#"pg_2419@tree6023", # Aves (Europe). Thuiller et al. 2011. Nature *NEW* other studies have more inclusive sampling
#"pg_435@tree5995",  # Aves. Brown et al. 2008. BMC Biol. *NEW* overruled by newer studies

]
